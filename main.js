'use strict';
document.addEventListener('DOMContentLoaded', ev => {
	const form = document.querySelector('.form');
	const inputs = document.querySelectorAll('.input');
	const inputsErrorHelpers = document.querySelectorAll('.input + .error-helper');
	const validEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	form.addEventListener('submit', e => {
		let validated = true;
		e.preventDefault();
		inputs.forEach((input, index) => {
			if(input.value.trim() === ''){
				input.classList.add('error-input');
				inputsErrorHelpers[index].classList.add('error-helper--active');
				validated = validated && false;
			}
			else{
				input.classList.remove('error-input');
				inputsErrorHelpers[index].classList.remove('error-helper--active');	
				validated = validated && true;
			}
			if(input.type === 'email'){
				validated = validated && validEmail.test(input.value);
			}
		});
		(validated)? form.submit(): console.log({validated});
	})
});
